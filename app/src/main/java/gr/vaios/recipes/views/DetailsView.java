package gr.vaios.recipes.views;

import com.hannesdorfmann.mosby.mvp.MvpView;

import gr.vaios.recipes.models.Recipe;

/**
 *
 * <p>
 *     Defines the methods of {@link gr.vaios.recipes.ui.DetailsActivity} that can be called by
 *     {@link gr.vaios.recipes.presenters.DetailsPresenter}.
 * </p>
 *
 * Created by Vaios on 7/29/16.
 */
public interface DetailsView extends MvpView {

    /**
     * <p>
     *     Set the values of recipes that was retrieved to the views of
     *     {@link gr.vaios.recipes.ui.DetailsActivity}.
     * </p>
     * @param recipe The recipe that was retrieved
     */
    void setRecipeDetails(Recipe recipe);

    /**
     * <p>Hides the progress bar which is being displayed during loading data.</p>
     */
    void stopProgressBar();

}
