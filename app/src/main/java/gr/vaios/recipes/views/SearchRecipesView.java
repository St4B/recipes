package gr.vaios.recipes.views;

import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.ArrayList;

import gr.vaios.recipes.models.Recipe;

/**
 *
 * <p>
 *     Defines the methods of {@link gr.vaios.recipes.ui.SearchRecipesActivity} that can be called
 *     by {@link gr.vaios.recipes.presenters.SearchRecipesPresenter}.
 * </p>
 *
 * Created by Vaios on 7/29/16.
 */
public interface SearchRecipesView extends MvpView {

    /**
     * <p>Adds a batch of recipes to the view that displays the results.</p>
     * @param recipes A batch of recipes.
     */
    void addResults(ArrayList<Recipe> recipes);

    /**
     * <p>Clears the view which displays the recipes search results.</p>
     */
    void clearResults();

    /**
     * <p>Shows a view which informs the user that more results are loading.</p>
     */
    void indicateLoading();

    /**
     * <p>Shows a view which informs the user that application is searching for recipes.</p>
     */
    void indicateSearching();

    /**
     * <p>Shows a view which informs the user that no results were found.</p>
     */
    void indicateNoResults();

    /**
     * <p>Hides the view which informs the user about searching state.</p>
     */
    void hideIndication();

}
