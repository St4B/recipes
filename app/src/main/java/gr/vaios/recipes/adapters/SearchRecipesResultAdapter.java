package gr.vaios.recipes.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import gr.vaios.recipes.R;
import gr.vaios.recipes.models.Recipe;

/**
 *
 * <p>
 *     An adapter that handles the display of a {@link Recipe} in a ListView. Also it helps us to
 *     add the recipes in batches. This is needed because Food2Fork API loads the search results
 *     in batches. As a result we may load a batch of results and when we scroll to the end of the
 *     ListView we can call the API again in order to get the next batch. The new batch must be
 *     added to the dataset of adapter. This happens with a call to {@link #addData(ArrayList)}.
 * </p>
 *
 * Created by Vaios on 7/31/16.
 */
public class SearchRecipesResultAdapter extends BaseAdapter {

    private ArrayList<Recipe> mRecipes = new ArrayList<>();

    private LayoutInflater mInflater;

    public SearchRecipesResultAdapter(Activity activity){
        mInflater = activity.getLayoutInflater();
    }

    /**
     * <p>Adds data to adapter in batches.</p>
     * @param recipes The new batch of recipes that will be added.
     */
    public void addData(ArrayList<Recipe> recipes){
        mRecipes.addAll(recipes);
        notifyDataSetChanged();
    }

    public void clearData(){
        mRecipes.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mRecipes.size();
    }

    @Override
    public Recipe getItem(int position) {
        return mRecipes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_result, null);
            holder = new ViewHolder();
            holder.mImage = (ImageView) convertView.findViewById(R.id.ivRecipeResult);
            holder.mTitle = (TextView) convertView.findViewById(R.id.tvRecipeTitle);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Recipe recipe = getItem(position);

        holder.mTitle.setText(recipe.getTitle());

        Picasso.with(convertView.getContext()).load(recipe.getImageUrl()).into(holder.mImage);

        return convertView;
    }

    private static class ViewHolder{

        ImageView mImage;

        TextView mTitle;

    }
}
