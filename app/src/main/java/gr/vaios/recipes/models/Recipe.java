package gr.vaios.recipes.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Vaios on 7/29/16.
 */
public class Recipe implements Parcelable {

    private String publisher;

    private String f2f_url;

    private String title;

    private String source_url;

    private String recipe_id;

    private String image_url;

    private String social_rank;

    private String publisher_url;

    private ArrayList<String> ingredients;

    public Recipe(){

    }

    public String getPublisher(){
        return publisher;
    }

    public String getFood2ForkUrl(){
        return f2f_url;
    }

    public String getTitle(){
        return title;
    }

    public String getSourceUrl(){
        return source_url;
    }

    public String getRecipeId(){
        return recipe_id;
    }

    public String getImageUrl(){
        return image_url;
    }

    public String getSocialRank(){
        return social_rank;
    }

    public String getPublisherUrl(){
        return publisher_url;
    }

    public ArrayList<String> getIngredients(){
        return ingredients;
    }

    protected Recipe(Parcel in) {
        publisher = in.readString();
        f2f_url = in.readString();
        title = in.readString();
        source_url = in.readString();
        recipe_id = in.readString();
        image_url = in.readString();
        social_rank = in.readString();
        publisher_url = in.readString();
        ingredients = in.createStringArrayList();
    }

    public static final Creator<Recipe> CREATOR = new Creator<Recipe>() {
        @Override
        public Recipe createFromParcel(Parcel in) {
            return new Recipe(in);
        }

        @Override
        public Recipe[] newArray(int size) {
            return new Recipe[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(publisher);
        dest.writeString(f2f_url);
        dest.writeString(title);
        dest.writeString(source_url);
        dest.writeString(recipe_id);
        dest.writeString(image_url);
        dest.writeString(social_rank);
        dest.writeString(publisher_url);
        dest.writeStringList(ingredients);
    }
}
