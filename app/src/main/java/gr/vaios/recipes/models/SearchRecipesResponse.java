package gr.vaios.recipes.models;

import java.util.ArrayList;

/**
 *
 * <p>
 *     It is needed in order to let GSON convert the Http Response of Food2Fork Api's search method
 *     to an ArrayList of {@link Recipe} Objects.
 *
 *     The response has the according format:
 *     <pre>
 *     {
 *      "count":1,
 *      "recipes":
 *          [
 *          {
 *              "publisher": "Publisher's name",
 *              "f2f_url": "http://www.google.com",
 *              "ingredients":
 *                  [
 *                      "1 ingredient",
 *                      "2 ingredients"
 *                  ],
 *              "source_url": "http://www.google.com",
 *              "recipe_id": "123",
 *              "image_url": "http://www.google.com",
 *              "social_rank": 73.89,
 *              "publisher_url": "http://www.google.com",
 *              "title": "Recipe's title"
 *          }
 *          ]
 *     }
 *     </pre>
 *
 *     As a result, GSON needs an object that will have an ArrayList<{@link Recipe}> property with
 *     the name recipe in order to convert the response and map the data.
 * </p>
 *
 * Created by Vaios on 7/31/16.
 */
public class SearchRecipesResponse {

    private int count;

    private ArrayList<Recipe> recipes;

    /**
     * <p>Get the recipes that was retrieved from the call of Food2Fork's Api search method.</p>
     * @return The Recipe
     */
    public ArrayList<Recipe> getRecipes(){
        return recipes;
    }

}
