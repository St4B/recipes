package gr.vaios.recipes.models;

/**
 *
 * <p>
 *     It is needed in order to let GSON convert the Http Response of Food2Fork's Api get method to
 *     a {@link Recipe} Object.
 *
 *     The response has the according format:
 *     <pre>
 *     {
 *      "recipe":
 *          {
 *              "publisher": "Publisher's name",
 *              "f2f_url": "http://www.google.com",
 *              "ingredients":
 *                  [
 *                      "1 ingredient",
 *                      "2 ingredients"
 *                  ],
 *              "source_url": "http://www.google.com",
 *              "recipe_id": "123",
 *              "image_url": "http://www.google.com",
 *              "social_rank": 73.89,
 *              "publisher_url": "http://www.google.com",
 *              "title": "Recipe's title"
 *          }
 *     }
 *     </pre>
 *
 *     As a result, GSON needs an object that will have a {@link Recipe} property with the name
 *     recipe in order to convert the response and map the data.
 * </p>
 *
 * Created by Vaios on 7/31/16.
 */
public class GetRecipeResponse {

    private Recipe recipe;

    /**
     * <p>Get the recipe that was retrieved from the call of Food2Fork's Api get method.</p>
     * @return The Recipe
     */
    public Recipe getRecipe(){
        return recipe;
    }
}
