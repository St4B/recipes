package gr.vaios.recipes.mvpImp;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby.mvp.MvpView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Vaios on 4/19/16.
 */
public abstract class BaseMvpPresenter<V extends MvpView> extends MvpBasePresenter<V> {

    protected EventBus mEventBus = EventBus.getDefault();

    @Override
    public void attachView(V view) {
        super.attachView(view);
        mEventBus.register(this);
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        mEventBus.unregister(this);
    }

    @Subscribe
    public final void onRegisterEvent(registerEvent event){
    }

    private final class registerEvent{}
}
