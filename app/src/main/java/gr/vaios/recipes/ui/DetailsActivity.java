package gr.vaios.recipes.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.Bind;
import gr.vaios.recipes.R;
import gr.vaios.recipes.managers.navigation.NavigationManager;
import gr.vaios.recipes.models.Recipe;
import gr.vaios.recipes.mvpImp.BaseMvpActivity;
import gr.vaios.recipes.presenters.DetailsPresenter;
import gr.vaios.recipes.views.DetailsView;

/**
 * Created by Vaios on 7/29/16.
 */
public class DetailsActivity extends BaseMvpActivity<DetailsView, DetailsPresenter>
        implements DetailsView {

    @Bind(R.id.pbLoading) protected ProgressBar mProgressBar;

    @Bind(R.id.lvIngredients) protected ListView mListView;

    @Bind(R.id.ivImageDetails) protected ImageView mImageView;

    @Bind(R.id.tvStickyView) protected TextView mStickyTextView;

    private View stickySpacer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        LayoutInflater inflater = getLayoutInflater();
        View listHeader = inflater.inflate(R.layout.list_details_parallax_header, null);
        stickySpacer = listHeader.findViewById(R.id.stickyPlaceholder);

        mListView.addHeaderView(listHeader);
    }

    @NonNull
    @Override
    public DetailsPresenter createPresenter() {
        Bundle extras = getIntent().getExtras();
        String recipeId = extras.getString(NavigationManager.BUNDLE_KEY_RECIPE_ID);
        return new DetailsPresenter(recipeId);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_details;
    }

    @Override
    public void setRecipeDetails(Recipe recipe) {
        setTitle(recipe.getTitle());

        Picasso.with(this).load(recipe.getImageUrl()).into(mImageView);

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(
                        this, R.layout.list_details_item, recipe.getIngredients());

        mStickyTextView.setVisibility(View.VISIBLE);
        mListView.setAdapter(adapter);
        mListView.addFooterView(createFooter(recipe));
        mListView.setOnScrollListener(mParallaxScrollListener);
    }

    private View createFooter(Recipe recipe){
        LayoutInflater inflater = getLayoutInflater();
        View footer = inflater.inflate(R.layout.list_details_footer, null);

        TextView publisher = (TextView) footer.findViewById(R.id.tvPublisherName);
        publisher.setText(recipe.getPublisher());

        TextView socialRank = (TextView) footer.findViewById(R.id.tvSocialRank);
        socialRank.setText(recipe.getSocialRank());

        Button btnInstructions = (Button) footer.findViewById(R.id.btnViewInstructions);
        btnInstructions.setOnClickListener(mOpenUrl);

        Button btnOriginal = (Button) footer.findViewById(R.id.btnViewOriginal);
        btnOriginal.setOnClickListener(mOpenUrl);

        return footer;
    }

    /**
     * <p>OnSrollListener that helps to create the parallax effect</p>
     */
    private OnScrollListener mParallaxScrollListener = new OnScrollListener() {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            //Do nothing
        }

        @Override
        public void onScroll(
                AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (view.getFirstVisiblePosition() == 0) {
                View firstChild = view.getChildAt(0);
                int topY = 0;
                if (firstChild != null) {
                    topY = firstChild.getTop();
                }

                int heroTopY = stickySpacer != null ? stickySpacer.getTop() : 0;

                mStickyTextView.setY(Math.max(0, heroTopY + topY));

                mImageView.setY(topY * 0.5f);
            }else{
                //First item is not visible. Set sticky TextView on top
                mStickyTextView.setY(0);
            }
        }
    };

    private View.OnClickListener mOpenUrl = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            String url = getUrl(v.getId());
            NavigationManager.goToBrowser(DetailsActivity.this, url);
        }

        /**
         * Get the
         * @param viewId
         * @return
         */
        public String getUrl(int viewId){
            switch (viewId){
                case R.id.btnViewInstructions:
                    return presenter.getInstructionsUrl();
                case R.id.btnViewOriginal:
                    return presenter.getOriginalUrl();
                default:
                    return getString(R.string.NoValidUrl);
            }
        }

    };

    @Override
    public void stopProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }
}
