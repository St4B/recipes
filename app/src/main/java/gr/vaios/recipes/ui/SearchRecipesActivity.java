package gr.vaios.recipes.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnItemClick;
import gr.vaios.recipes.R;
import gr.vaios.recipes.adapters.SearchRecipesResultAdapter;
import gr.vaios.recipes.managers.navigation.NavigationManager;
import gr.vaios.recipes.models.Recipe;
import gr.vaios.recipes.mvpImp.BaseMvpActivity;
import gr.vaios.recipes.presenters.SearchRecipesPresenter;
import gr.vaios.recipes.views.SearchRecipesView;

/**
 * Created by Vaios on 7/29/16.
 */
public class SearchRecipesActivity extends BaseMvpActivity<SearchRecipesView, SearchRecipesPresenter>
        implements SearchRecipesView {

    @Bind(R.id.svSearchRecipes) protected SearchView mSearchView;

    @Bind(R.id.lvResults) protected ListView mListView;

    @Bind(R.id.tvInfo) protected TextView mIndicatorText;

    @Bind(R.id.pbInfo) protected ProgressBar mIndicatorProgressBar;

    @Bind(R.id.llInfo) protected LinearLayout mIndicatorLayout;

    private SearchRecipesResultAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mSearchView.setOnQueryTextListener(mSearchClickListener);
        mListView.setOnScrollListener(mDetectEndScrollListener);
        mAdapter = new SearchRecipesResultAdapter(this);
        mListView.setAdapter(mAdapter);
    }

    @NonNull
    @Override
    public SearchRecipesPresenter createPresenter() {
        return new SearchRecipesPresenter();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_search;
    }

    private OnQueryTextListener mSearchClickListener = new OnQueryTextListener() {

        @Override
        public boolean onQueryTextSubmit(String query) {
            mAdapter.clearData();
            presenter.search(query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }

    };

    @Override
    public void addResults(ArrayList<Recipe> recipes) {
        mAdapter.addData(recipes);
    }

    @Override
    public void clearResults() {
        mAdapter.clearData();
    }

    @Override
    public void indicateLoading() {
        mIndicatorLayout.setVisibility(View.VISIBLE);
        mIndicatorProgressBar.setVisibility(View.VISIBLE);
        mIndicatorText.setText(getString(R.string.LoadingMore));
    }

    @Override
    public void indicateSearching() {
        mIndicatorLayout.setVisibility(View.VISIBLE);
        mIndicatorProgressBar.setVisibility(View.VISIBLE);
        mIndicatorText.setText(getString(R.string.Searching));
    }

    @Override
    public void indicateNoResults() {
        mIndicatorLayout.setVisibility(View.VISIBLE);
        mIndicatorProgressBar.setVisibility(View.GONE);
        mIndicatorText.setText(getString(R.string.NoResults));
    }

    @Override
    public void hideIndication() {
        mIndicatorLayout.setVisibility(View.GONE);
    }

    @OnItemClick(R.id.lvResults)
    public void goToDetails(int position){
        Recipe recipe = mAdapter.getItem(position);
        NavigationManager.goToDetails(this, recipe);
    }

    private AbsListView.OnScrollListener mDetectEndScrollListener =
            new AbsListView.OnScrollListener() {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == SCROLL_STATE_IDLE) {
                if (mListView.getLastVisiblePosition() >= mListView.getCount() - 1) {
                    presenter.getNextPage();
                }
            }
        }

        @Override
        public void onScroll(AbsListView view,
                             int firstVisible, int visibleCount, int totalCount) {
            //Do nothing
        }

    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        presenter.restoreState(savedInstanceState);
    }
}
