package gr.vaios.recipes.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import gr.vaios.recipes.R;
import gr.vaios.recipes.managers.navigation.NavigationManager;

/**
 * Created by v.tsitsonis on 1/8/2016.
 */
public class BrowserActivity extends AppCompatActivity {

    private WebView mWebView;

    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);
        mProgressBar = (ProgressBar) findViewById(R.id.pbLoading);
        mWebView = (WebView) findViewById(R.id.wvLoadUrl);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(mWebViewClient);
        Bundle extras = getIntent().getExtras();
        String url = extras.getString(NavigationManager.BUNDLE_KEY_URL);
        mWebView.loadUrl(url);
    }

    private WebViewClient mWebViewClient = new WebViewClient(){

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mProgressBar.setVisibility(View.GONE);
        }

    };
}
