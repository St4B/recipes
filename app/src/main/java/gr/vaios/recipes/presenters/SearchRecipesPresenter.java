package gr.vaios.recipes.presenters;

import org.greenrobot.eventbus.Subscribe;

import android.os.Bundle;

import java.util.ArrayList;

import gr.vaios.recipes.events.RequestFailedEvent;
import gr.vaios.recipes.events.SearchRecipesEvent;
import gr.vaios.recipes.managers.httpApi.HttpApiManager;
import gr.vaios.recipes.managers.httpApi.retrofitDelegation.RetrofitHttpManager;
import gr.vaios.recipes.models.Recipe;
import gr.vaios.recipes.mvpImp.BaseMvpPresenter;
import gr.vaios.recipes.views.SearchRecipesView;

/**
 * Created by Vaios on 7/29/16.
 */
public class SearchRecipesPresenter extends BaseMvpPresenter<SearchRecipesView> {

    private String mKeyword = "";

    private Integer mPage = 1;

    private ArrayList<Recipe> mRecipes = new ArrayList<>();

    private boolean hasMoreResults = true;

    private HttpApiManager mHttpApiManager = RetrofitHttpManager.getInstance();

    private static final String KEY_STATE_KEYWORD = "keyword";

    private static final String KEY_STATE_PAGE = "page";

    private static final String KEY_STATE_RECIPES = "recipes";

    private static final String KEY_STATE_HAS_MORE_RESULTS = "hasMoreResults";

    public void search(String keyword){
        mKeyword = keyword;
        mPage = 1;
        getView().clearResults();
        mRecipes.clear();
        hasMoreResults = true;
        getView().indicateSearching();
        mHttpApiManager.searchRecipes(mKeyword, mPage);
    }

    public void restoreData() {
        if(mRecipes.size() > 0) getView().addResults(mRecipes);
    }

    public void getNextPage(){
        if(hasMoreResults) {
            mPage++;
            mHttpApiManager.searchRecipes(mKeyword, mPage);
            getView().indicateLoading();
        }
    }

    @Subscribe
    public final void onSearchRecipesEvent(SearchRecipesEvent event){
        ArrayList<Recipe> recipesRetrieved = event.getRecipes();
        mEventBus.removeStickyEvent(event);

        if(recipesRetrieved == null || recipesRetrieved.size() == 0){
            getView().indicateNoResults();
            return;
        }

        mRecipes.addAll(recipesRetrieved);

        hasMoreResults = recipesRetrieved.size() % 30 == 0;

        if(isViewAttached()) {
            getView().addResults(recipesRetrieved);
            getView().hideIndication();
        }
    }

    @Subscribe
    public final void onRequestFailed(RequestFailedEvent event){
        mEventBus.removeStickyEvent(event);
        getView().hideIndication();
    }

    /**
     * <p>
     *     Saves the data of {@link SearchRecipesPresenter} in the given bundle. This helps us to
     *     be have persistent presenter trough Activity's lifecycle. (To restore the data use
     *     {@link #restoreState(Bundle)}).
     * </p>
     * @param outState The bundle where we will save tha data
     */
    public void saveState(Bundle outState){
        outState.putString(KEY_STATE_KEYWORD, mKeyword);
        outState.putInt(KEY_STATE_PAGE, mPage);
        outState.putParcelableArrayList(KEY_STATE_RECIPES, mRecipes);
        outState.putBoolean(KEY_STATE_HAS_MORE_RESULTS, hasMoreResults);
    }

    /**
     * <p>
     *     Restores the data of {@link SearchRecipesPresenter} which was saved by
     *     {@link #saveState(Bundle)}.
     * </p>
     * @param savedInstanceState
     */
    public void restoreState(Bundle savedInstanceState){
        mKeyword = savedInstanceState.getString(KEY_STATE_KEYWORD);
        mPage = savedInstanceState.getInt(KEY_STATE_PAGE);
        mRecipes = savedInstanceState.getParcelableArrayList(KEY_STATE_RECIPES);
        hasMoreResults = savedInstanceState.getBoolean(KEY_STATE_HAS_MORE_RESULTS);
        restoreData();
    }

}
