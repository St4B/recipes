package gr.vaios.recipes.presenters;

import org.greenrobot.eventbus.Subscribe;

import gr.vaios.recipes.events.GetRecipeEvent;
import gr.vaios.recipes.events.RequestFailedEvent;
import gr.vaios.recipes.managers.httpApi.HttpApiManager;
import gr.vaios.recipes.managers.httpApi.retrofitDelegation.RetrofitHttpManager;
import gr.vaios.recipes.models.Recipe;
import gr.vaios.recipes.mvpImp.BaseMvpPresenter;
import gr.vaios.recipes.views.DetailsView;

/**
 * Created by Vaios on 7/29/16.
 */
public class DetailsPresenter extends BaseMvpPresenter<DetailsView> {

    private Recipe mRecipe;

    private String mRecipeId;

    private HttpApiManager mHttpApiManager = RetrofitHttpManager.getInstance();

    public DetailsPresenter(String recipeId) {
        mRecipeId = recipeId;
        loadRecipe();
    }

    public void loadRecipe() {
        mHttpApiManager.getRecipe(mRecipeId);
    }

    @Override
    public void attachView(DetailsView view) {
        super.attachView(view);
        if(mRecipe != null) setData();
    }

    @Subscribe
    public final void onGetRecipeEvent(GetRecipeEvent event){
        mRecipe = event.getRecipe();
        mEventBus.removeStickyEvent(event);

        if(isViewAttached()) setData();
    }

    /**
     * <p>Set the retrieved data to the view. Be sure that the view is attached.</p>
     */
    private void setData(){
        getView().setRecipeDetails(mRecipe);
        getView().stopProgressBar();
    }

    public String getInstructionsUrl(){
        return mRecipe.getFood2ForkUrl();
    }

    public String getOriginalUrl(){
        return mRecipe.getSourceUrl();
    }

    @Subscribe
    public final void onRequestFailed(RequestFailedEvent event){
        mEventBus.removeStickyEvent(event);
        getView().stopProgressBar();
    }
}
