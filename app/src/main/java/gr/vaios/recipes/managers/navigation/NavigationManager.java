package gr.vaios.recipes.managers.navigation;

import android.app.Activity;
import android.content.Intent;

import gr.vaios.recipes.models.Recipe;
import gr.vaios.recipes.ui.BrowserActivity;
import gr.vaios.recipes.ui.DetailsActivity;

/**
 *
 * <p>
 *     Helper class for navigating between activities. Basically with this class the creation
 *     of intents is centralized.
 * </p>
 *
 * Created by Vaios on 7/29/16.
 */
public class NavigationManager {

    public static final String BUNDLE_KEY_RECIPE_ID = "recipeId";

    public static final String BUNDLE_KEY_URL = "url";

    /**
     * <p>Go to {@link DetailsActivity}.</p>
     * @param activity The current activity
     * @param recipe The recipe which we want to see in more details.
     */
    public static void goToDetails(Activity activity, Recipe recipe){
        Intent detailsIntent = new Intent(activity, DetailsActivity.class);
        detailsIntent.putExtra(BUNDLE_KEY_RECIPE_ID, recipe.getRecipeId());
        activity.startActivity(detailsIntent);
    }

    /**
     * <p>Go to {@link BrowserActivity}.</p>
     * @param activity The current activity.
     * @param url The url which we want to open.
     */
    public static void goToBrowser(Activity activity, String url){
        Intent browserIntent = new Intent(activity, BrowserActivity.class);
        browserIntent.putExtra(BUNDLE_KEY_URL, url);
        activity.startActivity(browserIntent);
    }

}
