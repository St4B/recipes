package gr.vaios.recipes.managers.httpApi;

/**
 *
 * <p>
 *     Defines the operations that will be executed in order to retrieve data without being
 *     dependent to the underlying implementation. The methods do not need to return a result. The
 *     calls to the api will be asynchronous, so we must create a callback or use a bus in order to
 *     retrieve the results.
 * </p>
 *
 * Created by Vaios on 7/30/16.
 */
public interface HttpApiManager {

    /**
     * <p>Starts a search based on the given keyword and page of results.</p>
     */
    void searchRecipes(String keyword, Integer page);

    /**
     * <p>Starts the retrieval of recipe's details based on the given recipe id.</p>
     */
    void getRecipe(String recipeId);

}
