package gr.vaios.recipes.managers.httpApi.retrofitDelegation;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import gr.vaios.recipes.errorHandling.ErrorEvent;
import gr.vaios.recipes.events.GetRecipeEvent;
import gr.vaios.recipes.events.RequestFailedEvent;
import gr.vaios.recipes.events.SearchRecipesEvent;
import gr.vaios.recipes.managers.httpApi.HttpApiManager;
import gr.vaios.recipes.models.GetRecipeResponse;
import gr.vaios.recipes.models.Recipe;
import gr.vaios.recipes.models.SearchRecipesResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 *
 * <p>
 *     Implementation of {@link HttpApiManager} with the help of retrofit library.
 *     The RetrofitHttpManager follows the singleton pattern.
 *     Also it use EventBus in order to post results asynchronously.
 * </p>
 *
 * Created by Vaios on 7/30/16.
 */
public class RetrofitHttpManager implements HttpApiManager {

    private Food2ForkApi mFood2ForkApi;

    private EventBus mEventBus = EventBus.getDefault();

    private static final String API_KEY = "46b6f27446193b202fb5858f81111e89";//"b549c4c96152e677eb90de4604ca61a2";

    private static final String BASE_URL = "http://food2fork.com/api/";

    private static RetrofitHttpManager sRetrofitHttpManager;

    private RetrofitHttpManager(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mFood2ForkApi = retrofit.create(Food2ForkApi.class);
    }

    public static RetrofitHttpManager getInstance(){
        if(sRetrofitHttpManager == null) sRetrofitHttpManager = new RetrofitHttpManager();
        return sRetrofitHttpManager;
    }

    @Override
    public void searchRecipes(String keyword, Integer page) {
        Call<SearchRecipesResponse> call = mFood2ForkApi.search(API_KEY, keyword, page);
        call.enqueue(mSearchRecipesCallback);
    }

    @Override
    public void getRecipe(String recipeId) {
        Call<GetRecipeResponse> call = mFood2ForkApi.get(API_KEY, recipeId);
        call.enqueue(mGetRecipeCallback);
    }

    /**
     * <p>Callback to handle the response of {@link Food2ForkApi#get(String, String)}.</p>
     */
    private Callback<GetRecipeResponse> mGetRecipeCallback = new Callback<GetRecipeResponse>() {

        @Override
        public void onResponse(
                Call<GetRecipeResponse> call, Response<GetRecipeResponse> response) {
            if(response.isSuccessful()) {
                GetRecipeResponse getRecipeResponse = response.body();
                Recipe recipe = getRecipeResponse.getRecipe();
                mEventBus.postSticky(new GetRecipeEvent(recipe));
            }else{
                String errorMessage = "Response of http://food2fork.com/api/get has errors";
                mEventBus.postSticky(new ErrorEvent(errorMessage));
                mEventBus.postSticky(new RequestFailedEvent());
            }
        }

        @Override
        public void onFailure(Call<GetRecipeResponse> call, Throwable t) {
            mEventBus.postSticky(new ErrorEvent(t.getMessage()));
            mEventBus.postSticky(new RequestFailedEvent());
        }
    };

    /**
     * <p>Callback to handle the response of {@link Food2ForkApi#search(String, String, Integer)}.</p>
     */
    private Callback<SearchRecipesResponse> mSearchRecipesCallback =
            new Callback<SearchRecipesResponse>() {

        @Override
        public void onResponse(
                Call<SearchRecipesResponse> call, Response<SearchRecipesResponse> response) {
            if(response.isSuccessful()) {
                SearchRecipesResponse searchRecipesResponse = response.body();
                ArrayList<Recipe> recipes = searchRecipesResponse.getRecipes();
                mEventBus.postSticky(new SearchRecipesEvent(recipes));
            }else{
                String errorMessage = "Response of http://food2fork.com/api/search has errors";
                mEventBus.postSticky(new ErrorEvent(errorMessage));
                mEventBus.postSticky(new RequestFailedEvent());
            }
        }

        @Override
        public void onFailure(Call<SearchRecipesResponse> call, Throwable t) {
            mEventBus.postSticky(new ErrorEvent(t.getMessage()));
            mEventBus.postSticky(new RequestFailedEvent());

        }
    };

}
