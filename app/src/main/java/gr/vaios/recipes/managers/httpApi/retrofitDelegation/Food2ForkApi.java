package gr.vaios.recipes.managers.httpApi.retrofitDelegation;

import gr.vaios.recipes.models.GetRecipeResponse;
import gr.vaios.recipes.models.SearchRecipesResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 *
 * <p>
 *     Defines the food2fork's api methods that would be called by {@link RetrofitHttpManager}.
 * </p>
 *
 * Created by Vaios on 7/30/16.
 */
public interface Food2ForkApi {

    @GET("search")
    Call<SearchRecipesResponse> search(
            @Query("key") String key,@Query("q") String keyword, @Query("page")Integer page);

    @GET("get")
    Call<GetRecipeResponse> get(@Query("key") String key, @Query("rId") String recipeId);
}
