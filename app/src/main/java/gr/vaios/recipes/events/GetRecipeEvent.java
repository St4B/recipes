package gr.vaios.recipes.events;

import gr.vaios.recipes.models.Recipe;

/**
 *
 * <p>This event is used in order to get the details of recipe asynchronously.</p>
 *
 * Created by Vaios on 7/31/16.
 */
public class GetRecipeEvent {

    private Recipe mRecipe;

    /**
     * @param recipe The recipe that was retrieved.
     */
    public GetRecipeEvent(Recipe recipe){
        mRecipe = recipe;
    }

    /**
     * <p>Get the recipe that was retrieved.</p>
     * @return The recipe.
     */
    public Recipe getRecipe(){
        return mRecipe;
    }
}
