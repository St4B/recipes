package gr.vaios.recipes.events;

import java.util.ArrayList;

import gr.vaios.recipes.models.Recipe;

/**
 *
 *  <p>
 *      This event is used in order to get the a batch of recipe search results asynchronously.
 *      The results of a search return in batches by the Food2Fork API.
 *  </p>
 *
 * Created by Vaios on 7/31/16.
 */
public class SearchRecipesEvent {

    private ArrayList<Recipe> mRecipes;

    /**
     * @param recipes A batch of recipes that were retrieved.
     */
    public SearchRecipesEvent(ArrayList<Recipe> recipes){
        mRecipes = recipes;
    }

    /**
     * <p>Get the batch of recipes that were retrieved.</p>
     * @return The batch of recipes that were retrieved.
     */
    public ArrayList<Recipe> getRecipes(){
        return mRecipes;
    }
}
