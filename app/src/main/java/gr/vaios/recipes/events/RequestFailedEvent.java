package gr.vaios.recipes.events;

/**
 *
 *  <p>This event is used to be informed that a call to the Food2Fork Api has failed.</p>
 *
 * Created by v.tsitsonis on 2/8/2016.
 */
public class RequestFailedEvent {

}
