package gr.vaios.recipes.errorHandling;

import java.util.ArrayList;

/**
 *
 * <p>
 *     Event that will be posted when an error is occurred. Basically it will be posted in a catch
 *     block. This event will be received by {@link ErrorManager} which is responsible to display
 *     the errors.
 * </p>
 *
 * Created by Vaios on 4/30/16.
 */
public class ErrorEvent {

    /**
     * Keep the errors that are about to be posted.
     */
    private ArrayList<String> mErrors = new ArrayList<>();

    /**
     * A constructor that adds a list of error.
     * @param errors A list of errors that are about to be posted to {@link ErrorManager}.
     */
    public ErrorEvent(ArrayList<String> errors){
        mErrors = errors;
    }

    /**
     * A constructor that adds only one error.
     * @param error An error that is about to be posted to {@link ErrorManager}.
     */
    public ErrorEvent(String error){
        mErrors.add(error);
    }

    /**
     * Retrieves a list of errors that were posted with this event.
     * @return The errors that were posted.
     */
    public ArrayList<String> getErrors(){
        return mErrors;
    }
}
