package gr.vaios.recipes.errorHandling;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import gr.vaios.recipes.R;

/**
 *
 * <p>Basic AlertDialog builder for {@link ErrorManager}.</p>
 *
 * Created by Vaios on 4/30/16.
 */
public class AlertDialogImp implements IAlertBuilder {

    @Override
    public void build(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.ErrorTitle));
        builder.setMessage(message);
        builder.setPositiveButton(context.getString(R.string.ErrorButtonOk), mOkClick);
        builder.setCancelable(false);
        builder.show();
    }

    private OnClickListener mOkClick = new OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    };
}
