package gr.vaios.recipes.errorHandling;

import android.content.Context;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 *
 * <p>
 *     Manager that handles the displaying of error messages. This manager follows the Singleton
 *     Design Pattern.
 * </p>
 * <p>
 *     It provides the ability to handle errors that are occurred while the Application is in the
 *     background. If the Application is in the background errors are added in a queue. Then they
 *     will be displayed when the ErrorManager is attached. If an event is posted and the
 *     ErrorManager is attached then the errors are displayed immediately.
 * </p>
 * <p>
 *     The addition of error messages happens when an {@link ErrorEvent} is posted.
 * </p>
 * <p>
 *     The creation of AlertDialog is defined by an {@link IAlertBuilder} implementation.
 * </p>
 * <p>
 *     Warning!!! Do not forget to call {@link #detach()} otherwise memory leak may occur.
 * </p>
 *
 * Created by Vaios on 4/27/16.
 */
public class ErrorManager {

    /**
     * Keeps the instance of {@link ErrorManager}.
     */
    private static ErrorManager sErrorManager;

    /**
     * Bus implementation in order to retrieve the {@link ErrorEvent} that are posted.
     */
    private EventBus mEventBus;

    /**
     * Keeps the errors that are occurred while the Application is in the background.
     */
    private PriorityQueue<String> mErrors = new PriorityQueue<>();


    private boolean isAttached = false;

    /**
     * The context to which the ErrorManager is attached. It is used in order to build the
     * AlertDialogs.
     */
    private Context mContext;

    /**
     * Delegation of AlertDialog building. Provides the ability to have different AlertDialogs
     * implementations.
     */
    private IAlertBuilder mAlertBuilder = new AlertDialogImp();

    /**
     * Private constructor as a part of singleton implementation. {@link ErrorManager} can only be
     * created by {@link #newInstance()}.
     */
    private ErrorManager(){
        mEventBus = EventBus.getDefault();
        mEventBus.register(this);
    }

    /**
     * Retrieves the instance of {@link ErrorManager}.
     * @return
     */
    public static ErrorManager newInstance(){
        if(sErrorManager == null) sErrorManager = new ErrorManager();
        return sErrorManager;
    }

    /**
     * Attach {@link ErrorManager} to a context. Usually this context is an Activity. Do not forget
     * to call {@link #detach()}, otherwise it is possible for a memory leak to occur.
     * @param context
     */
    public void attach(Context context){
        mContext = context;
        isAttached = true;
        if(mErrors.size() > 0) showErrorMessages();
    }

    /**
     * Creates AlertDialog for every error that there is in {@link #mErrors}.
     */
    public void showErrorMessages(){
        while (mErrors.size() > 0) {
            String error = mErrors.element();
            mAlertBuilder.build(mContext, error);
            mErrors.remove();
        }
    }

    /**
     * Detaches the {@link ErrorManager} from Context. It must be called before Context is destroyed.
     */
    public void detach(){
        mContext = null;
        isAttached = false;
    }

    /**
     * According to EventBus implementation this method is called in order to receive a posted
     * {@link ErrorEvent}. On retrieval of event the errors are added to {@link #mErrors} and if
     * ErrorManager is attached they are displayed to user.
     * @param event
     */
    @Subscribe
    public final void onErrorEvent(ErrorEvent event){
        ArrayList<String> errors = event.getErrors();
        mErrors.addAll(errors);
        mEventBus.removeStickyEvent(event);
        if(isAttached) showErrorMessages();
    }

    /**
     * <p>Returns the errors that were not displayed. It is useful in order to save the errors
     * before the OS kill the Application due to low memory while it is in the background.</p>
     * @return A PriorityQueue of the errors that were not displayed.
     */
    public PriorityQueue<String> getErrors(){
        return mErrors;
    }
}
