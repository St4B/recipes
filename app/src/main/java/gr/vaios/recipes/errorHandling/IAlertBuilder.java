package gr.vaios.recipes.errorHandling;

import android.content.Context;

/**
 *
 * <p>
 *     Interface that let us to define the AlertDialog that will be used by {@link ErrorManager} in
 *     order to display error messages.
 * </p>
 *
 * Created by Vaios on 4/30/16.
 */
public interface IAlertBuilder {

    /**
     * Creates and displays an AlertDialog.
     * @param context The context to which the AlertDialog will be attached.
     * @param message The message that will display the AlertDialog.
     */
    void build(Context context, String message);

}
